package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;

public class PaymentActivity extends AppCompatActivity {

    TextView movieName, movieDate, movieTheatre, moviePrice;
    AppCompatButton Btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        movieName = findViewById(R.id.movieName);
        movieDate = findViewById(R.id.movieDate);
        movieTheatre = findViewById(R.id.movieTheatre);
        moviePrice = findViewById(R.id.moviePrice);
        Btn = findViewById(R.id.paymentBtn);

        Intent i = getIntent();
        String name = i.getStringExtra("MovieName");
        String date = i.getStringExtra("MovieDate");
        String theatre = i.getStringExtra("MovieTheatre");
        String price = "300/-";

        movieName.setText(name);
        movieDate.setText(date);
        movieTheatre.setText(theatre);
        moviePrice.setText(price);

        Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference dbref = FirebaseDatabase.getInstance().getReference();
                DatabaseReference newRef = dbref.child("Bookings").push();

                HashMap<String,String> hashMap = new HashMap<>();
                hashMap.put("userId",uid);
                hashMap.put("MovieName",name);
                hashMap.put("MovieDate",date);
                hashMap.put("Theatre", theatre);
                hashMap.put("Price","300/-");

                newRef.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            Toast.makeText(PaymentActivity.this, "Payment successful", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(PaymentActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
    }

}