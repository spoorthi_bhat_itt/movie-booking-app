package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

public class MoviesDisplayActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies_display);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        MovieData[] movieData = new MovieData[]{
               new MovieData("RRR","4th April 2022","PVR Cinemas",R.drawable.rrr),
                new MovieData("The Kashmir Files","4th April 2022","PVR Cinemas",R.drawable.thekashmirfiles),
                new MovieData("The Batman","4th April 2022","DRC Cinemas",R.drawable.batman),
                new MovieData("RRR","5th April 2022","PVR Cinemas",R.drawable.rrr),
                new MovieData("The Kashmir Files","5th April 2022","DRC Cinemas",R.drawable.thekashmirfiles),
                new MovieData("The Batman","5th April 2022","PVR Cinemas",R.drawable.batman),
                new MovieData("RRR","6th April 2022","DRC Cinemas",R.drawable.rrr),
                new MovieData("RRR","7th April 2022","PVR Cinemas",R.drawable.rrr),
                new MovieData("The Kashmir Files","6th April 2022","PVR Cinemas",R.drawable.thekashmirfiles),
                new MovieData("The Batman","6th April 2022","DRC Cinemas",R.drawable.batman),
                new MovieData("RRR","8th April 2022","PVR Cinemas",R.drawable.rrr),
                new MovieData("The Kashmir Files","7th April 2022","DRC Cinemas",R.drawable.thekashmirfiles),
                new MovieData("The Batman","7th April 2022","PVR Cinemas",R.drawable.batman),
                new MovieData("RRR","9th April 2022","DRC Cinemas",R.drawable.rrr)
        };

        MovieAdapter movieAdapter= new MovieAdapter(movieData,MoviesDisplayActivity.this);
        recyclerView.setAdapter(movieAdapter);
    }
}