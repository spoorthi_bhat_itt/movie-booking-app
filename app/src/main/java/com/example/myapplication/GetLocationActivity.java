package com.example.myapplication;

import static android.content.ContentValues.TAG;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatSpinner;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class GetLocationActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    Spinner spinner;
    AppCompatButton savebtn;

    //array of strings to store locations
    String[] loc = { "Mysore", "Banglore" };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_location);


        spinner = findViewById(R.id.spinner);
        savebtn = findViewById(R.id.savebtn);

        //tells which spinner item is clicked
        spinner.setOnItemSelectedListener(this);

        ArrayAdapter ad = new ArrayAdapter(GetLocationActivity.this,android.R.layout.simple_spinner_item,loc);
        ad.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

      
        spinner.setAdapter(ad);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        Toast.makeText(this, loc[i], Toast.LENGTH_SHORT).show();

        //when the button is clicked "save", update user location
        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                DatabaseReference dbref = FirebaseDatabase.getInstance().getReference("Users");
                dbref.child(uid).child("Locality").setValue(loc[i]);
                Toast.makeText(GetLocationActivity.this, "Location set", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(GetLocationActivity.this,MoviesDisplayActivity.class);
                startActivity(i);
            }
        });





    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}