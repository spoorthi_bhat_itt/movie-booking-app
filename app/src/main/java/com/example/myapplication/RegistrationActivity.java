package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Objects;

public class RegistrationActivity extends AppCompatActivity {

    private TextInputEditText usernameEdt, emailEdt ,passEdt, conpassEdt;
    private Button registerBtn;
    private ProgressBar PBloading;
    private TextView LoginTextView;

    //instance of firebase authentication
    private FirebaseAuth mAuth;

    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registeration);

        //initialise UI Elements
        usernameEdt = findViewById(R.id.register_EDTusername);
        emailEdt = findViewById(R.id.register_EDTemail);
        passEdt = findViewById(R.id.register_EDTpass);
        conpassEdt = findViewById(R.id.register_EDTconpass);
        registerBtn = findViewById(R.id.register_btn);
        LoginTextView = findViewById(R.id.Login_user_textView);
        PBloading = findViewById(R.id.PBloading);

        //Get firebase instance
        mAuth = FirebaseAuth.getInstance();

        //OnClick TextView, got to LoginActivity
        LoginTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        //OnClicking Register Button --> Validate, Authenticate and add to firebase
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //make progressbar visible
                PBloading.setVisibility(View.VISIBLE);

                //get values for fields and validate
                String username = usernameEdt.getText().toString();
                String email = emailEdt.getText().toString();
                String pass = passEdt.getText().toString();
                String conpass = conpassEdt.getText().toString();

                //check if pass == confirmPass, any empty fields
                if(!pass.equals(conpass)){
                    Toast.makeText(RegistrationActivity.this, "Password does not match!", Toast.LENGTH_SHORT).show();
                }else if(TextUtils.isEmpty(username) || TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)) {
                    Toast.makeText(RegistrationActivity.this, "Please Enter all your credentials!", Toast.LENGTH_SHORT).show();
                }else{
                    // register user
                    register(username,email,pass);
                }
            }
        });

    }

    private void register(String username, String email, String pass) {

        // email and password authentication
        mAuth.createUserWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                //if authentication successful then add to realtime database
                if(task.isSuccessful()){

                    //get instance of current user
                    FirebaseUser rUser = mAuth.getCurrentUser();
                    //get userId of current user
                    assert rUser != null;
                    String userId = rUser.getUid();
                    //get db instance , locate "Users" and userId
                    databaseReference = FirebaseDatabase.getInstance().getReference("Users").child(userId);

                    //populate hashMap with "Users" child attributes
                    HashMap<String,String> hashMap = new HashMap<>();
                    hashMap.put("userId",userId);
                    hashMap.put("userName",username);
                    hashMap.put("Password",pass);
                    hashMap.put("Email",email);
                    hashMap.put("Locality","None");

                    databaseReference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if(task.isSuccessful()){
                                PBloading.setVisibility(View.GONE);
                                Toast.makeText(RegistrationActivity.this, "Registration successful", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(RegistrationActivity.this,LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }else{
                                Toast.makeText(RegistrationActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }
                    });


                }else{
                    //if can't register
                    PBloading.setVisibility(View.GONE);
                    Toast.makeText(RegistrationActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        });
    }
}