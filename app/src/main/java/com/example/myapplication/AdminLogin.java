package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

public class AdminLogin extends AppCompatActivity {
    private TextInputEditText emailEdt,passEdt;
    private Button loginBtn;
    private ProgressBar PBloading;
    private TextView UserLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_login);

        emailEdt = findViewById(R.id.ad_login_EDTemail);
        passEdt = findViewById(R.id.ad_login_EDTpass);
        loginBtn = findViewById(R.id.ad_login_btn);
        PBloading = findViewById(R.id.PBloading);
        UserLogin = findViewById(R.id.user_loginbtn);

        //go to user login page
        UserLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent login = new Intent(AdminLogin.this,LoginActivity.class);
                startActivity(login);
            }
        });

        //check if admin
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PBloading.setVisibility(View.VISIBLE);

                String email = emailEdt.getText().toString();
                String pass = passEdt.getText().toString();

                if(TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)) {
                    PBloading.setVisibility(View.GONE);
                    Toast.makeText(AdminLogin.this, "Please Enter all your credentials!", Toast.LENGTH_SHORT).show();
                }else{
                    // login user
                    login(email,pass);
                }

            }
        });
    }

    private void login(String email, String pass) {

        if(email.equals("admin@gmail.com") && pass.equals("admin09")){
            PBloading.setVisibility(View.GONE);
            Toast.makeText(this, "Welcome Login", Toast.LENGTH_SHORT).show();
            Intent k = new Intent(AdminLogin.this,AdminMain.class);
            startActivity(k);
        }else{
            PBloading.setVisibility(View.GONE);
            Toast.makeText(this, "Incorrect email or password!", Toast.LENGTH_SHORT).show();
        }

    }
}