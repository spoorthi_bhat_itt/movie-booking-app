package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText emailEdt,passEdt;
    private Button loginBtn;
    private ProgressBar PBloading;
    private TextView RegisterTextView, admin;

    //instance of firebase authentication
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        emailEdt = findViewById(R.id.login_EDTemail);
        passEdt = findViewById(R.id.login_EDTpass);
        loginBtn = findViewById(R.id.login_btn);
        PBloading = findViewById(R.id.PBloading);
        RegisterTextView = findViewById(R.id.register_user_textView);
        admin = findViewById(R.id.admin);

        //Get firebase instance
        mAuth = FirebaseAuth.getInstance();


        //OnClick TextView, got to Registration Activity
        RegisterTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(i);
            }
        });

        //Login as Admin
        admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent j = new Intent(LoginActivity.this, AdminLogin.class);
                startActivity(j);
            }
        });

        //On clicking login button
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //make progressbar visible
                PBloading.setVisibility(View.VISIBLE);

                String email = emailEdt.getText().toString();
                String pass = passEdt.getText().toString();

                if(TextUtils.isEmpty(email) || TextUtils.isEmpty(pass)) {
                    Toast.makeText(LoginActivity.this, "Please Enter all your credentials!", Toast.LENGTH_SHORT).show();
                }else{
                    // login user
                    login(email,pass);
                }
            }
        });

    }

    private void login(String email, String pass) {

        //signing in with email and password
        mAuth.signInWithEmailAndPassword(email, pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if(task.isSuccessful()){
                    PBloading.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, "Login successful", Toast.LENGTH_SHORT).show();


                    Intent intent = new Intent(LoginActivity.this,GetLocationActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }else{
                    PBloading.setVisibility(View.GONE);
                    Toast.makeText(LoginActivity.this, Objects.requireNonNull(task.getException()).getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}