package com.example.myapplication;

public class MovieData {

    private String movieName;
    private String movieDate;
    private String movieTheatre;
    private Integer movieImage;

    public MovieData(String movieName, String movieDate, String movieTheatre, Integer movieImage) {
        this.movieName = movieName;
        this.movieDate = movieDate;
        this.movieTheatre = movieTheatre;
        this.movieImage = movieImage;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public String getMovieDate() {
        return movieDate;
    }

    public void setMovieDate(String movieDate) {
        this.movieDate = movieDate;
    }

    public String getMovieTheatre() {
        return movieTheatre;
    }

    public void setMovieTheatre(String movieTheatre) {
        this.movieTheatre = movieTheatre;
    }

    public Integer getMovieImage() {
        return movieImage;
    }

    public void setMovieImage(Integer movieImage) {
        this.movieImage = movieImage;
    }
}
