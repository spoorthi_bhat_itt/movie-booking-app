package com.example.myapplication;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.ViewHolder>{

    MovieData[] movieData;
    Context context;
    public MovieAdapter(MovieData[] movieData,MoviesDisplayActivity activity) {
        this.movieData = movieData;
        this.context = activity;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.movie_item_list,parent,false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final MovieData  movieDataList = movieData[position];
        holder.textName.setText(movieDataList.getMovieName());
        holder.textDate.setText(movieDataList.getMovieDate());
        holder.textTheatre.setText(movieDataList.getMovieTheatre());
        holder.movieImage.setImageResource(movieDataList.getMovieImage());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                Toast.makeText(context, movieDataList.getMovieName(), Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context,PaymentActivity.class);
                i.putExtra("MovieName",movieDataList.getMovieName());
                i.putExtra("MovieDate",movieDataList.getMovieDate());
                i.putExtra("MovieTheatre",movieDataList.getMovieTheatre());
                context.startActivity(i);


            }
        });
    }

    @Override
    public int getItemCount() {
        return movieData.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView movieImage;
        TextView textName;
        TextView textDate,textTheatre;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            movieImage = itemView.findViewById(R.id.imageView);
            textName = itemView.findViewById(R.id.textname);
            textDate = itemView.findViewById(R.id.textDate);
            textTheatre = itemView.findViewById(R.id.textTheatre);

        }
    }
}
